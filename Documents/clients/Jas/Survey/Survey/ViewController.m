//
//  ViewController.m
//  Survey
//
//  Created by Kunal Singh on 30/09/16.
//  Copyright © 2016 SilstoneGroup. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController (){
    
    int ans1,ans2,ans3,ans4,ans5,ans6,ans7;
    NSString *ques1,*ques2,*ques3,*ques4,*ques5,*ques6,*ques7;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [scrollView setScrollEnabled:YES];
    [scrollView setContentSize:CGSizeMake(320, 1250)];
}

- (IBAction)btnRadioQ1:(UIButton*)sender {
    
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 11:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 12:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 13:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 14:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 15:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques1 = @"How severe is your knee stiffness after first wakening in the morning";
    ans1 = [self setAnswer:(int)tappedBtn.tag];
    
}
- (IBAction)btnRadioQ2:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 21:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 22:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 23:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 24:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 25:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques2 = @"Twisting/pivoting on your knee";
    ans2 = [self setAnswer:(int)tappedBtn.tag];
    
}

- (IBAction)btnRadioQ3:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 31:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 32:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 33:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 34:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 35:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques3 =@"Straightening knee fully";
    ans3 = [self setAnswer:(int)tappedBtn.tag];
    
}

- (IBAction)btnRadioQ4:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 41:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 42:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 43:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 44:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 45:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques4 = @"Going up or down stairs";
    ans4 = [self setAnswer:(int)tappedBtn.tag];
    
}

- (IBAction)btnRadioQ5:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 51:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 52:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 53:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 54:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 55:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques5 = @"Standing upright";
    ans5 = [self setAnswer:(int)tappedBtn.tag];
    
}

- (IBAction)btnRadioQ6:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 61:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 62:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 63:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 64:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 65:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    ques6 = @"Rising from sitting";
    ans6 = [self setAnswer:(int)tappedBtn.tag];
    
}

- (IBAction)btnRadioQ7:(id)sender {
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        switch (radButton.tag) {
            case 71:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 72:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 73:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 74:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            case 75:
                [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
    
    UIButton *tappedBtn = (UIButton *)sender;
    [tappedBtn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    
    ques7 = @"Bending to floor/pick up an object";
    ans7 = [self setAnswer:(int)tappedBtn.tag];
}

- (IBAction)btnSubmit:(id)sender {
    
    if (ans1 == 0) {
        [self showAlertWithtitle:@"Please answer Question 1"];
        return;
    }else if (ans2 == 0) {
        [self showAlertWithtitle:@"Please answer Question 2"];
        return;
        
    }else if (ans3 == 0){
        [self showAlertWithtitle:@"Please answer Question 3"];
        return;
        
    }else if (ans4 == 0) {
        [self showAlertWithtitle:@"Please answer Question 4"];
        return;
        
    }else if (ans5 == 0){
        [self showAlertWithtitle:@"Please answer Question 5"];
        return;
        
    }else if (ans6 == 0) {
        [self showAlertWithtitle:@"Please answer Question 6"];
        return;
        
    }else if (ans7 == 0) {
        [self showAlertWithtitle:@"Please answer Question 7"];
        return;
        
    }

    NSLog(@"%@ , %d",ques1,ans1);
    NSLog(@"%@ , %d",ques2,ans2);
    NSLog(@"%@ , %d",ques3,ans3);
    NSLog(@"%@ , %d",ques4,ans4);
    NSLog(@"%@ , %d",ques5,ans5);
    NSLog(@"%@ , %d",ques6,ans6);
    NSLog(@"%@ , %d",ques7,ans7);
    NSMutableArray *answerArray = [[NSMutableArray alloc]init];
    [answerArray addObject:[NSNumber numberWithInt:ans1-1]];
      [answerArray addObject:[NSNumber numberWithInt:ans2-1]];
     [answerArray addObject:[NSNumber numberWithInt:ans3-1]];
     [answerArray addObject:[NSNumber numberWithInt:ans4-1]];
    [answerArray addObject:[NSNumber numberWithInt:ans5-1]];
      [answerArray addObject:[NSNumber numberWithInt:ans6-1]];
       [answerArray addObject:[NSNumber numberWithInt:ans7-1]];
    [self PostJson:answerArray];
    
}


-(void)PostJson:(NSMutableArray *)answersArray {
    
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
     [del showProgressHUD:self.view :@"please wait"];
    __block NSMutableDictionary *resultsDictionary;
    
    NSMutableDictionary *userDictionary = [[NSMutableDictionary alloc] init];
    [userDictionary setValue:@"koosjr" forKey:@"type"];
    [userDictionary setValue:answersArray forKey:@"responses"];
     NSMutableArray *answerArrayVal = [[NSMutableArray alloc]init];
    [answerArrayVal addObject:userDictionary];
    if ([NSJSONSerialization isValidJSONObject:answerArrayVal]) {//validate it
        NSError* error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:answerArrayVal options:NSJSONWritingPrettyPrinted error: &error];
        NSURL* url = [NSURL URLWithString:@"http://li668-152.members.linode.com:3000/api/patient/PT-0H-80-AR/evaluations"];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        [request setHTTPMethod:@"POST"];//use POST
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-length"];
        [request setHTTPBody:jsonData];//set data
        __block NSError *error1 = [[NSError alloc] init];
        
        //use async way to connect network
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
        {

           if ([data length]>0 && error == nil) {
                resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
                NSLog(@"resultsDictionary is %@",resultsDictionary);
              
               [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey"
                                                                   message:@"answers have been successfully submitted."
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                   [alert show];
                    [self clearData];
                  [del hideProgressHUD];
                   
               }];
               
                
            } else if ([data length]==0 && error ==nil) {
                NSLog(@" download data is null");
                [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey"
                                                                    message:@"answers have been successfully submitted."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    [self clearData];
                    //Your code goes in here
                    [del hideProgressHUD];
                    
                }];
            } else if( error!=nil) {
                NSLog(@" error is %@",error);
                [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey"
                                                                message:@"error in submission."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                    }];
            }
        }];
    }
}
-(void)showAlertWithtitle:(NSString *)Message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey"
                                                    message:Message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}

-(int)setAnswer:(int)answerTag{
    
    NSString *ansString = [NSString stringWithFormat:@"%d",(int)answerTag];
    
    NSString *string = [ansString substringFromIndex: [ansString length] - 1];
    return [string intValue];
}

-(void)clearData {
    ans1 = @""; ans2 =@""; ans3 =@""; ans4 =@""; ans5 =@""; ans6 =@""; ans7 =@"";
    
    NSArray *viewsToRemove = [scrollView subviews];
    
    for (UIButton *radButton in viewsToRemove) {
        
        if ([radButton isKindOfClass:[UIButton class]]) {
            if (radButton.tag == 100) {
                
            }else{
            [radButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            }

        }
    }
}

@end


