//
//  main.m
//  Survey
//
//  Created by Kunal Singh on 30/09/16.
//  Copyright © 2016 SilstoneGroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
