//
//  ViewController.h
//  Survey
//
//  Created by Kunal Singh on 30/09/16.
//  Copyright © 2016 SilstoneGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    
    __weak IBOutlet UIScrollView *scrollView;
    
    
}

- (IBAction)btnRadioQ1:(UIButton*)sender;

- (IBAction)btnRadioQ2:(UIButton*)sender;

- (IBAction)btnRadioQ3:(UIButton*)sender;

- (IBAction)btnRadioQ4:(UIButton*)sender;

- (IBAction)btnRadioQ5:(UIButton*)sender;

- (IBAction)btnRadioQ6:(UIButton*)sender;

- (IBAction)btnRadioQ7:(UIButton*)sender;
- (IBAction)btnSubmit:(id)sender;

@end

