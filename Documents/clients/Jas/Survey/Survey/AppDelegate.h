//
//  AppDelegate.h
//  Survey
//
//  Created by Kunal Singh on 30/09/16.
//  Copyright © 2016 SilstoneGroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MBProgressHUD *HUD;
}
@property (strong, nonatomic) UIWindow *window;

-(void)showProgressHUD:(UIView*)view :(NSString *)Text;

-(void)hideProgressHUD;
@end

