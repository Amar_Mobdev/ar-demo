﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	public float waitTime;

	// Use this for initialization
	void Start () 
	{
		Invoke ("loadScene",waitTime);
	}
	
	void loadScene () 
	{
		SceneManager.LoadScene (1);
	}
}
